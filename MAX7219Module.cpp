#include <MAX7219Module.h>

void MAX7219Module::set(byte address, byte data) {
	byte bytes[] = {address, data};
	this->transfer(bytes, sizeof(bytes) / sizeof(byte));
}

MAX7219Module::MAX7219Module(byte sckPin, byte mosiPin, byte misoPin, byte ssPin) :
SPIModule::SPIModule(sckPin, mosiPin, misoPin, ssPin) {
}

MAX7219Module::MAX7219Module() :
MAX7219Module::MAX7219Module(SCK, MOSI, MISO, SS) {
}

void MAX7219Module::initial() {
	this->setOrder(SPIModule::ORDER_MSB);
	this->setPolarity(SPIModule::POLARITY_HIGH);
	this->setPhase(SPIModule::PHASE_END);
	SPIModule::initial();
}

void MAX7219Module::setDecoding(byte data) {
	this->set(0x09, data);
}

void MAX7219Module::setBrightness(byte data) {
	this->set(0x0A, data & 0x0F);
}

void MAX7219Module::setBits(byte data) {
	this->set(0x0B, data & (LENGTH - 1));
}

void MAX7219Module::setEnabled(bool data) {
	this->set(0x0C, data);
}

void MAX7219Module::setMode(bool data) {
	this->set(0x0F, data);
}

void MAX7219Module::setRow(byte row, byte data) {
	this->set((row & (LENGTH - 1)) + 1, data);
}

void MAX7219Module::clear() {
	for (byte i = 0; i < MAX7219Module::LENGTH; i++) {
		this->setRow(i, 0x00);
	}
}