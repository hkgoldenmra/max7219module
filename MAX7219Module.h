#ifndef MAX7219MODULE_H
#define MAX7219MODULE_H

#include <SPIModule.h>

class MAX7219Module : public SPIModule {
	private:
		void set(byte, byte);
	public:
		const static byte LENGTH = 8;
		const static byte DECODING_NONE = 0x00;
		const static byte DECODING_D0 = 0x01;
		const static byte DECODING_D3 = 0x0F;
		const static byte DECODING_D7 = 0xFF;
		const static bool MODE_NORMAL = false;
		const static bool MODE_TEST = true;
		MAX7219Module(byte, byte, byte, byte);
		MAX7219Module();
		void initial();
		void setDecoding(byte);
		void setBrightness(byte);
		void setBits(byte);
		void setEnabled(bool);
		void setMode(bool);
		void setRow(byte, byte);
		void clear();
};

#endif