#include <MAX7219Module.h>
#include <DS1302Module.h>

const byte VCC_PIN = 6;
const byte GND_PIN = 5;
MAX7219Module max7219Module = MAX7219Module(2, 4, 4, 3);
const byte DELAY_MS = 100;

void setup() {
	Serial.begin(9600);
	pinMode(VCC_PIN, OUTPUT);
	digitalWrite(VCC_PIN, HIGH);
	pinMode(GND_PIN, OUTPUT);
	digitalWrite(GND_PIN, LOW);
	max7219Module.initial();
	max7219Module.setDecoding(MAX7219Module::DECODING_NONE);
	max7219Module.setBrightness(0x00);
	max7219Module.setBits(0x07);
	max7219Module.setEnabled(true);
	max7219Module.setMode(MAX7219Module::MODE_NORMAL);
	max7219Module.clear();
}

void loop() {
	for (byte i = 0; i < 14; i++) {
		for (byte j = 0; j < 8; j++) {
			byte s = abs((i % 7) - j);
			if (i >= 7) {
				s = 7 - s;
			}
			max7219Module.setRow(j, 1 << s);
		}
		delay(DELAY_MS);
	}
}